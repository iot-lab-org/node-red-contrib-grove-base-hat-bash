#!/bin/bash
# use gpio for parsing commands
# $1 is BMC pinnumber $2 is 0 to 1024
# using pwm on specific ports (18)
gpio -g mode $1 pwm
gpio -g pwm $1 $2
exit 0

