#!/bin/bash
# using io_tools io_groveusr <Port number>

# check if command exists
command -v io_groveusr >/dev/null 2>&1 || { echo >&2 "I require io_groveusr but it's not installed. Aborting."; exit 1; }

io_groveusr $1
exit 0
