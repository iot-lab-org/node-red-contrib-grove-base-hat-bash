#!/bin/bash
# read the analog value from grove base hat with STM32F030F4P6TR (Address 0x04)
# and the MM32F031F6P6 (Address 0x08)
# This code automaticaly detect the available address
# Leo Korbee | 2023-09-20
#
# i2c-tools should be installed to use i2cget
# $1 is 0, 2, 4 or 6 -> 

# check i2c-addres of Grove-Base-HAT
if i2cget -a -y 1 0x04 >/dev/null  2>&1
then 
#  echo "Grove Base Hat found at 0x04"

if [[ $1 == 0 ]]
then
  hexNum=`i2cget -a -y 1 0x04 0x30 w`
fi

if [[ $1 == 2 ]]
then
  hexNum=`i2cget -a -y 1 0x04 0x32 w`
fi

if [[ $1 == 4 ]]
then
  hexNum=`i2cget -a -y 1 0x04 0x34 w`
fi

if [[ $1 == 6 ]]
then
  hexNum=`i2cget -a -y 1 0x04 0x36 w`
fi

fi

if i2cget -a -y 1 0x08 >/dev/null 2>&1
then
#echo "Grove base hat found at 0x08"

if [[ $1 == 0 ]]
then
  # push twice for word measurement?
  i2cset -y 1 0x08 0x30
  i2cset -y 1 0x08 0x30
  hexNum=`i2cget -y 1 0x08 0x30 w`
fi

if [[ $1 == 2 ]]
then
  i2cset -y 1 0x08 0x32
  i2cset -y 1 0x08 0x32
  hexNum=`i2cget -y 1 0x08 0x32 w`
fi

if [[ $1 == 4 ]]
then
  i2cset -y 1 0x08 0x34
  i2cset -y 1 0x08 0x34
  hexNum=`i2cget -y 1 0x08 0x34 w`
fi

if [[ $1 == 6 ]]
then
  i2cset -y 1 0x08 0x36
  i2cset -y 1 0x08 0x36
  hexNum=`i2cget -y 1 0x08 0x36 w`
fi

fi

hexNum=${hexNum#*0x}   # remove prefix ending in "0x"

echo $(( 16#$hexNum ))
