#!/bin/bash
# using io_tools io_dht22 <Port number>

# check if command exists
command -v io_dht11 >/dev/null 2>&1 || { echo >&2 "I require io_dht11 but it's not installed. Aborting."; exit 1; }

io_dht11 $1
exit 0

