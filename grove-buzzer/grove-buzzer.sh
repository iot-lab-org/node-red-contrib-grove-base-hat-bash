#!/bin/bash
# use gpio for parsing commands
# $1 is BMC pinnumber $2 is 0 or 1
gpio -g mode $1 out

if [[ $2 > 0 ]];
then
   for (( i=1; i<=5; i++ ))
   do
      gpio -g write $1 1
      sleep 0.2
      gpio -g write $1 0
      sleep 0.2
   done
fi
exit 0

