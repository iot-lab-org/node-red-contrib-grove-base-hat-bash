var path = require('path');

module.exports = function(RED) {


    function GroveLcdRgbBacklightNode(config) {
        RED.nodes.createNode(this,config);

        this.port_name = "I2C";
        this.status({fill:"blue",shape:"dot",text:this.port_name});

        var node = this;

        var red;
        var green;
        var blue;

        node.on('input', function(msg) {

	    var parameter = '';

	    if (msg.red >= 0 && msg.red <= 255)
            {
            	red = msg.red;
            }

            if (msg.green >= 0 && msg.green <= 255)
            {
               green = msg.green;
            }

            if (msg.blue >= 0 && msg.blue <= 255)
            {
               blue = msg.blue;
            }

	    if (msg.payload != undefined)
	    {
		parameter += '\"' + msg.payload + '\"';
            }

	    if (msg.line2 != undefined)
	    {
		parameter = parameter + ' \"' + msg.line2 + '\"';
	    }

	    if (red != undefined && green != undefined && blue != undefined)
	    {
	       parameter = parameter + ' ' + red + ' ' + green + ' ' + blue;
	    }

	    console.log(parameter);

            // msg.payload = msg.payload.toLowerCase();
            const exec = require('child_process').exec;
            // const message = msg.payload.message;
            // const message = msg.payload;  // Changed simple string payload in v.0.0.4
            this.status({fill:"yellow",shape:"ring",text:this.port_name + " connecting"});
            exec('bash ' + path.join( __dirname , 'grove-lcd-rgb-backlight.sh') + ' ' + parameter, (err, stdout, stderr) => {
                if (err) { console.log(err); }
                // console.log(stdout);
                this.status({fill:"green",shape:"dot",text:this.port_name + " connected"});
            });
            
            // node.send(msg);
        });
    }
    RED.nodes.registerType("grove-lcd-rgb-backlight",GroveLcdRgbBacklightNode);
}
