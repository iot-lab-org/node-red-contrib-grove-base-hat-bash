#!/bin/bash
# using io_tools io_i2clcdrgb <text line 1> [<text line 2] [<r> <g> <b>]

# check if command exists
command -v io_i2clcdrgb >/dev/null 2>&1 || { echo >&2 "I require io_i2clcdrgb but it's not installed. Aborting."; exit 1; }

len=$#

if [ $len -eq 1 ];
then 
	io_i2clcdrgb "$1"
fi

if [ $len -eq 2 ];
then 
	io_i2clcdrgb "$1" "$2"
fi

if [ $len -eq 4 ];
then 
	io_i2clcdrgb "$1" "$2" "$3" "$4"
fi

if [ $len -eq 5 ];
then 
	io_i2clcdrgb "$1" "$2" "$3" "$4" "$5"
fi

exit 0
