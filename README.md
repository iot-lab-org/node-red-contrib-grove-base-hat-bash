# node-red-contrib-grove-base-hat-bash

Nodes for Grove Base HAT on Raspberry Pi using bash scripts and other tools

## Note
This node uses WiringPi, i2c-tools and io_toolsPi for reading and writing data from/to I/O pins 
and I2C-ADC converter and specific hardware sensors or actuators. 

Updating and adding nodes for Grove Base HAT on Raspberry Pi at the time of 2023/09/02


The nodes support all Grove Base HAT for Raspberry Pi (beginning 2023-09-20). All test are done with:

- Grove Base HAT for Raspberry Pi Zero v1.0 with STM32F030F4P6TR processor. 
- Grove Base HAT for Raspberry Pi v1.0 with MM32F031F6P6 microcontroller.

The (analog) nodes automaticaly detects the type of HAT installed.


## Supported sensors/actuators
At this moment I try to make all **sensors** and **actuators** from the "GrovePi+ Starters Kit" available.
This Kit was produced by Seeed and Dexter industries.

These actuators and sensors where availabe in the GrovePi+ Starters Kit:

OUTPUT (Actuators)
- grove-led (input number 0 or 1)
- grove-buzzer (input > 0 (number), will bleep 5 times on any input :-)
- grove-relay (input number 0 or 1)
- grove-led-dimmable (input number 0 - 1024), will shine more or less bright
- grove-lcd-rgb-backlight (input RGB value and text), will work with hardware modification

INPUT (Sensors)
- grove-button (output number 0 or 1 on button change)
- grove-light-sensor-v1_2 (input trigger number, output 0-1000 represents 0-100.0%)
- grove-rotary-angle-sensor (input trigger number, output 0-1000 represents 0-100% turn)
- grove-temperature-humidity-sensor (input trigger number, output JSON temperatureC, temperatureF and humidity)
- grove-ultrasonic-ranger (input trigger number, output 0~350 respresents distance in cm, NaN when distance is tool long)
- grove-sound-sensor (input trigger number, ouput 0-1000 relative. Default value (silence) is about 500. Sounds makes the amplitude grow.

## Extra supported Actuators/Sensors

OUTPUT (Actuators)


INPUT (Sensors)
- grove-temperature-humidity-sensor-pro (input trigger number, output JSON temperatureC, temperatureF and humidity)


## The Grove Base HAT
https://www.seeedstudio.com/Grove-Base-Hat-for-Raspberry-Pi.html

![grove-base-hat](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-base-hat.jpeg)

## Install and Setup

### 1. Setup Grove Base HAT

Connect the Grove Base HAT to the Raspberry Pi.

### 2. Enable I2C interface on Raspberry Pi.

Run `sudo raspi-config` and select `3 Interface Options`. Then select `I5 I2C` and enable the ARM I2C interface (`<YES>`).

This is a screenshot of the config menu:

![raspi-config-i2c.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/raspi-config-i2c.png)


### 3. Check i2c-tools installed and I2C connection

run this command (all versions of Grove Base HAT)

```
i2cdetect -a -y 1
```

And you should get this response (STM32F030F4P6TR microcontroller)

```
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00: -- -- -- -- 04 -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

```
run this command (post 2020? versions of Grove Base HAT)

```
i2cdetect -y 1
```

And you should get this response (MM32F031F6P6 microcontroller)

```
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:                         08 -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --   

```

When you get 08 you have a newer board and the scripts should automaticaly use the 0x08 address.

### 4. Install WiringPi library

Run the following commands to install the wiring Pi library:

```

cd

git clone https://github.com/WiringPi/WiringPi.git

cd WiringPi

./build


```

### 5. Install io_toolsPi
These tools are used for specific hardware, like DHT11/22, ultrasonic ranger, and LCD RGB display etc. 
Be shure that the WiringPi library is installed first. These tools depend on it.

Run the following command to install the io_toolsPi:

```

cd

git clone https://gitlab.com/iot-lab-org/io_toolsPi

cd io_toolsPi

./build

```

### 6. Install Grove Base HAT (bash) nodes

Execute following command on the commandline in your home directory: `npm install node-red-contrib-grove-base-hat-bash`. 
The nodes will be installed in "/home/`<username>`/.node-red/node_modules/node-red-contrib-grove-base-hat-bash".

OR

Adding **node-red-contrib-grove-base-hat-bash** nodes to the palette with the GUI interface.

https://nodered.org/docs/user-guide/runtime/adding-nodes


## How to use the nodes

### grove - led

Connect **(Grove) LED Socket Kit v1.5** to Grove Base HAT's **Digital (D)** port. Inject number 1 to switch it on 
and inject number 0 to switch it off. 

![grove-led.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-led.png)

### grove - relay

Connect **(Grove) Relay v1.2** to Grove Base HAT's **Digital (D)** port. Inject number 1 to switch it on
and inject number 0 to switch it off.

![grove-relay.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-relay.png)

### grove - buzzer

Connect **(Grove) Buzzer v1.2** to Grove Base HAT's **Digital (D)** port. Inject number 1 to bleep 5 times.

![grove-buzzer.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-buzzer.png)

### grove - led - dimmable

Connect **(Grove) LED Socket Kit v1.5** (All colors) to Grove Base HAT's **PWM port (PWM)** and inject number 0-1000. However the range from 0 to ~300 will work. 
Above this value you will not see any difference.

![grove-led-dimmable.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-led-dimmable.png)

### grove - lcd - rgb - backlight

Connect **Grove-LCD RGB Backlight V4.0** to a Grove Base HAT's **I2C port (I2C)**. 
Version 4.0 of the LCD rbg display only will show the text on the display when the power is connected to 5V.
By default the Grove Base HAT connectors supply 3.3V. To solve this problem you can carefully loosen the red cable and mini clamp
from the original connector. Then isolate it with tape or a single header enclosure and connect it to physical pin nr 2.
This is how it should look like:

![grove-lcd-rgb-backlight-connector.jpeg](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-lcd-rgb-backlight-connector.jpeg)

Then create an inject as input:

![grove-lcd-rgb-backlight.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-lcd-rgb-backlight.png)

And set this information as input:

![grove-lcd-rgb-backlight-input.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-lcd-rgb-backlight-input.png)

When not all red, green and blue information is given the color will not change. By default the background color is white and
line2 information is not mandatory.

### grove - button

Connect **(Grove) Button v1.2** to Grove Base HAT's **Digital port (D)**. After the deployment a script will check every 0.1 second if the
button is pushed. The output will apear only on changes: pushed = 1, released = 0.

![grove-button.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-button.png)

### grove - light - sensor - v1_2

Connect **Grove - Light sensor V1.2 01/20/2016** or **(Grove) Light sensor v1.1** to Grove Base HAT's **Analog port (A)**. When a number is injected the output will appear. 
This will be a value between 0 and 1000, representing 0.0% - 100.0%.

![grove-light-sensor-v1_2.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-light-sensor-v1_2.png)

### grove - rotatry - angle - sensor

Connect **(Grove) Rotary Angle sensor (P) V1.2** to Grove Base HAT's **Analog port (A)**. When a number is injected the output will appear. 
This will be a value between 0 and 1000, representing 0.0% - 100.0% rotarary.

![grove-rotary-angle-sensor.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-rotary-angle-sensor.png)

### grove - temperature - humidity - sensor

Connect **(Grove) Temperture&Humidity Sensor v1.2** to Grove Base HAT's **Digital port (D)**. When a number is injected the output will appear.
This will be a JSON Object with temperature in degrees Celcius and degrees Fahrenheit and Relative Humidity. 
You can extract the specific value by using a function and select the value by code like `msg.payload = msg.payload.temperatureC`.
The values will be whole numbers due to accuracy of the sensor. 

![grove-temperature-humidity-sensor.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-temperature-humidity-sensor.png)

### grove - ultrasonic - ranger

Connect **Grove Ultrasonic Ranger V2.0** to Grove Base HAT's **Digital port (D)**. When a number is injected the output will appear.
This wil be the distance in cm. When NaN appears the sensor data is out of range.

![grove-ultrasonic-ranger.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-ultrasonic-ranger.png)

### grove - sound - sensor

Connect **(Grove) Sound Sensor v1.6** to Grove Base HAT's **Analog port (A)**. When a number is injected the output will appear. 
This will be a value between 0 and 1000, representing 0.0% - 100.0% sound. Usualy output will be around 500 and the amplitude of the sound
will be the output. So you will need another functions to measure the amplitude or min-max information.

![grove-sound-sensor.png](https://gitlab.com/iot-lab-org/node-red-contrib-grove-base-hat-bash/-/raw/main/images/grove-sound-sensor.png)


### grove - temperature - humidity - sensor - pro

Connect **(Grove) Temperture&Humidity Sensor Pro** to Grove Base HAT's **Digital port (D)**. When a number is injected the output will appear.
This will be a JSON Object with temperature in degrees Celcius and degrees Fahrenheit and Relative Humidity. 
You can extract the specific value by using a function and select the value by code like `msg.payload = msg.payload.temperatureC`.
The values will be with one digit behind the dot (tenth) due to accuracy of the sensor. 

Same node setup as grove-temperature-humidity-sensor.


## Examples

With the menu options in Node-RED: import, tab Examples. You can find under the **node-red-contrib-grove-base-hat-bash** a
"All Node Test Examples" flow. In this flow all nodes are configured for testing. Remember that some noded use the same input/output port.
Alway configure the right port for the connected sensor/actuator.


## Technical background
The Grove Base HAT does connect the I/O pins directly to the connectors.
D5 etc, I2C, SPI etc. However the Raspberry Pi does not contain analog input/output. 
The Grove Base HAT contains a microcontroller (STM32F030F4P6TR or MM32F031F6P6) that does the 12 bit ADC conversion (max number 4096).

The controller sets this information on the I2C bus on address 0x04 or 0x08:

- The register 10+channel contains the 12 bit number 
- The register 20+channel contains the voltage (in mV)
- The register 30+channel contains the relative number to the supply voltage (3V3) 0-1000 = 0-100.0%
- The register 29 contains the supply voltage

The return value is 2 bytes long

Instead of using Python scripts, that are deprecated due to Python version and library change, 
I use bash scripts that use diffent tools available on the Raspberry Pi. 
I added C-based code tools with the io_toolsPi library.

WiringPi is used to access digital pins and i2c-tools (default on Rasberry Pi OS)  are used to read the i2c chip and analog data.
So these tools should be installed before you can use these nodes. 

Please use the debug and log tools when the node does not work. I used for example `node-red-log` to get the error messages.


## Tested on/with

### Hardware

Rasberry Pi 3B+

Grove Base HAT first version (with STM32F030F4P6TR microcontroller) and 
Grove Base HAT new version (with MM32F031F6P6 microcontroller).


### Software

The Raspberry Pi OS version this is tested is: Release date 2023-05-03

`Linux <hostname> 6.1.21-v7+ #1642 SMP Mon Apr  3 17:20:52 BST 2023 armv7l GNU/Linux`

The Node-Red version is v3.0.2

The WiringPi version is (gpio version) 2.70

The latest io_tools (version unknown)

## Thanks
Many thanks to Seigo Tanaka for the original version of these Nodes. 
The original work can be found here: https://github.com/1ft-seabass/node-red-contrib-grove-base-hat


## Information/contact
Please look at https://www.iot-lab.org/blog/1135/ for information or contact.

When you like this library please Rate this library at https://flows.nodered.org/node/node-red-contrib-grove-base-hat-bash or send me a Coffee https://ko-fi.com/iotlab.

With regards, Leo.

