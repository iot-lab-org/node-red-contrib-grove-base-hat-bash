#!/bin/bash
# use gpio for parsing commands
# $1 is BMC pinnumber $2 is 0 or 1
gpio -g mode $1 out
gpio -g write $1 $2
exit 0

