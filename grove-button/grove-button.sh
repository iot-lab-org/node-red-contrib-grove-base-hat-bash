#!/bin/bash
# use gpio for parsing commands
gpio -g mode $1 in

PREVOUTPUT="0"

for (( ; ; ))
do
    OUTPUT=`gpio -g read $1`
    if [ $OUTPUT -ne $PREVOUTPUT ]
    then
       echo $OUTPUT
       PREVOUTPUT=$OUTPUT
    fi
   sleep 0.1
done

exit 0

